import React, { useContext, useReducer } from 'react';
import Realm from "realm";
import { appReducer } from '../helpers/reducer';

const ReducerContext = React.createContext(null);

let appRealm

export const ReducerProvider = ({ children }) => {

  if (appRealm !== undefined) {
    return appRealm
  }
    appRealm = new Realm.App({ id: "tasktracker-gortb", timeout: 10000 });

    const reducer = useReducer(appReducer, {loading: false, logged: false, user:null, app:null});
    const [{logged, user, loading}, dispatch] = reducer;

    const signIn = async (email, password) => {
        const creds = Realm.Credentials.emailPassword(email, password);
        const itsLogged = await appRealm.logIn(creds);

        if(itsLogged){
            dispatch({type:'logIn', payload:appRealm.currentUser})
        }else{
            console.log('could not loggin')
        }
      };

    const signUp = async (email, password) => {
        const register = await appRealm.emailPasswordAuth.registerUser(email, password);

        if(register){
            dispatch({type:'logIn', payload:appRealm.currentUser})
        }else{
            console.log('could not register')
        }
    };
    
      // The signOut function calls the logOut function on the currently
      // logged in user
      const signOut = () => {
        if (user == null) {
          console.warn("Not logged in, can't log out!");
          return;
        }
        user.logOut();
        dispatch({type:'logOut'})
      };

    return (
        <ReducerContext.Provider
          value={{
            signUp,
            signIn,
            signOut,
            user,
            loading, // list of projects the user is a memberOf
            logged,
            dispatch
          }}
        >
          {children}
        </ReducerContext.Provider>
      );
}

export const useRealm = () => {
  const auth = useContext(ReducerContext);
  if (auth == null) {
    throw new Error("useRealm() called outside of a ReducerContext?");
  }
  return auth;
};