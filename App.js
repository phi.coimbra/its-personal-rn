import React, {useEffect} from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {NavigationContainer, DrawerActions} from '@react-navigation/native';

import {View} from 'react-native';

import _ from 'lodash';
import {ReducerProvider} from './providers/ReducerProvider';
import {Perfil} from './pages/Perfil';
import {Login} from './pages/Login';
import {Signup} from './pages/Signup';
import {Recover} from './pages/Recover';

import Icon from 'react-native-vector-icons/FontAwesome';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();
const LoginStack = createStackNavigator();
const LoginPage = createStackNavigator();
const RootStack = createStackNavigator();
const Stack = createStackNavigator();

const App = () => {
  const createHomeStack = () => (
    <ReducerProvider>
      <Stack.Navigator headerShown={false}>
        <Stack.Screen
          name="Initial"
          children={LoginStackScreen}
          options={{headerShown: false}}
        />

        <Stack.Screen name="Home" children={RootStackScreen} />
      </Stack.Navigator>
    </ReducerProvider>
  );

  const LoginStackScreen = () => {
    return (
      <LoginStack.Navigator>
        <LoginPage.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <LoginPage.Screen
          name="Signup"
          component={Signup}
          options={{
            title: '',
          }}
        />
        <LoginPage.Screen
          name="Recover"
          component={Recover}
          options={{
            title: 'Recover',
          }}
        />
        <LoginPage.Screen
          name="Perfil"
          component={Perfil}
          options={{
            title: 'Perfil',
            header: ({scene, previous, navigation}) => {
              const {options} = scene.descriptor;
              const title =
                options.headerTitle !== undefined
                  ? options.headerTitle
                  : options.title !== undefined
                  ? options.title
                  : scene.route.name;

              return (
                <View
                  title={title}
                  leftButton={undefined}
                  style={options.headerStyle}
                />
              );
            },
          }}
        />
      </LoginStack.Navigator>
    );
  };

  const RootStackScreen = () => {
    return (
      <RootStack.Navigator
        //headerMode='screen'

        initialRouteName="Home"
        screenOptions={{
          gestureEnabled: true,
          cardOverlayEnabled: true,
          ...TransitionPresets.ModalPresentationIOS,
        }}
        mode="modal">
        <RootStack.Screen
          name="Home"
          children={createDrawer}
          options={({navigation, route}) => {
            //const { toggleDrawer } = props.navigation
            //console.log('props', route)
            return {
              //title: /*props.route.params.name*/'ok',
              headerShown:
                _.get(
                  navigation.dangerouslyGetState().routes[0],
                  'state.index',
                ) >= 1
                  ? false
                  : true,

              headerLeft: () => (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    paddingLeft: 10,
                  }}>
                  {_.get(
                    navigation.dangerouslyGetState().routes[0],
                    'state.index',
                  ) >=
                  1 /*_.has(navigation.dangerouslyGetState().routes[0].state, 'index')*/ ? (
                    <Icon.Button
                      size={25}
                      name="angle-left"
                      backgroundColor="transparent"
                      underlayColor="transparent" // This one
                      color="white"
                      onPress={() =>
                        navigation.navigate('Homes')
                      }></Icon.Button>
                  ) : (
                    <Icon.Button
                      size={25}
                      name="bars"
                      backgroundColor="transparent"
                      underlayColor="transparent" // This one
                      color="white"
                      onPress={() =>
                        navigation.dispatch(DrawerActions.toggleDrawer())
                      }></Icon.Button>
                  )}
                </View>
              ),
              headerRight: () => (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    width: 100,
                  }}>
                  <Icon.Button
                    size={24}
                    name="bell"
                    backgroundColor="transparent"
                    underlayColor="transparent" // This one
                    color="white"></Icon.Button>
                </View>
              ),
              title:
                _.get(
                  navigation.dangerouslyGetState().routes[0],
                  'state.index',
                ) >= 1
                  ? route.state.routeNames[route.state.index]
                  : route.name,
            };
          }}
        />
        <RootStack.Screen name="Perfil" component={Perfil} />
      </RootStack.Navigator>
    );
  };

  const createDrawer = props => (
    <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen
        name="Homes"
        options={{
          drawerLabel: 'Updates',
          headerTitle: 'ok',
          headerShown: false,
        }}
        component={Home}
      />
    </Drawer.Navigator>
  );

  return <NavigationContainer>{createHomeStack()}</NavigationContainer>;
};

export default App;
