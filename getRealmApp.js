import Realm from "realm";

let app;

// Returns the shared instance of the Realm app.
export function getRealmApp() {

  if (app === undefined) {
   
    const appId = "tasktracker-gortb"; // Set Realm app ID here.
    const appConfig = {
      id: appId,
      timeout: 10000,
      app: {
        name: "tasktracker",
        version: "0",
      },
    };
    app = new Realm.App(appConfig);
    //console.log()
    console.log('user --> ', JSON.stringify(app.currentUser))
  }
  return app;
}
