import React, { useEffect } from "react";
import "react-native-gesture-handler";
import { createStackNavigator } from "@react-navigation/stack";

import { Text, View } from "react-native";
import { Products, Task } from "./schemas";
import Realm from "realm";


const Drawer = createDrawerNavigator();
const LoginStack = createStackNavigator();


const App = () => {

  const appRealm = new Realm.App({ id: "tasktracker-gortb", timeout: 10000 });

  const credentials = Realm.Credentials.emailPassword('macabro23@hotmail.com','364999')

  const reducer = useReducer(logginReducer, {loading:false})
  const [{logged, cliente}, dispatch] = reducer;

  useEffect(() => {
    
    const realmInit = async () => {
      const realmdb = await getRealm()
      console.log(realmdb);

      if(realmdb){
  
      
      const syncTasks = realmdb.objects("Task");
      let sortedTasks = syncTasks.sorted("name");
      console.log('SORTED: --> ', sortedTasks.length)
      sortedTasks.addListener(() => {
        console.log('data2:', JSON.stringify(sortedTasks));
      })
    }
      
    }
    
    realmInit();
    
  },[])

  const getRealm = async () => {

    // loggedIn as anonymous user
    const user = await appRealm.logIn(credentials);

    console.log(user.id)
    
  
    const config = {
      schema: [Task.schema, Products.schema],
      sync: {
        user,
        partitionValue: `PUBLIC`,
      },
    };

    /*Realm.open(config).then((projectRealm) => {

      const syncTasks = projectRealm.objects("Task");
      let sortedTasks = syncTasks.sorted("name");

      console.log('SORTED: --> ', sortedTasks.length)

    });*/

    let realmDBSync = null

try{
    realmDBSync = await Realm.open(config)
}catch(err){
  console.log('erro')
}

    console.log('passou')
   
    return realmDBSync
  }



  createHomeStack = () =>
  <DB.Provider value = {myDB}>
    <Dispatch.Provider value = {reducer}>
      <Auth.Provider value = {clientAuth}>
        <Users.Provider value = {usersService}>
        
          <Stack.Navigator
          headerMode="none"
          >  
          {!logged? 
            <Stack.Screen
              name="Login"
              children={this.LoginStackScreen}
              options={{headerShown: false}}
            />
            :
            <Stack.Screen
              name="Home"
              children={this.RootStackScreen}
              />
              
          }
            <Stack.Screen
              name="Screens"
              children={this.ScreensStackScreen}
              />  
            
          </Stack.Navigator>
        </Users.Provider>
      </Auth.Provider>
    </Dispatch.Provider>
  </DB.Provider>

LoginStackScreen = () => {
  return (
    <LoginStack.Navigator
      
      >  

      <ScreensStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <ScreensStack.Screen
        name="Signup"
        component={Signup}
        options={{
          title: ""
        }}
      />
      <ScreensStack.Screen
        name="Recover"
        component={Recover}
        options={{
          title: "Recover"
        }}
      />
      <ScreensStack.Screen
        name="Perfiledit"
        component={Perfiledit}
        options={{
          title: "Perfil",
          header: ({ scene, previous, navigation }) => {
            const { options } = scene.descriptor;
            const title =
              options.headerTitle !== undefined
                ? options.headerTitle
                : options.title !== undefined
                ? options.title
                : scene.route.name;
          
            return (
              <View
                title={title}
                leftButton={
                  undefined
                }
                style={options.headerStyle}
              />
            );
          }
        }}
      />
     
  </LoginStack.Navigator>
)
}



  return (
    <View>
    <Text>INICIOU</Text>
   {/* <AuthProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Welcome View"
            component={WelcomeView}
            options={{ title: "Task Tracker" }}
          />
          <Stack.Screen
            name="Projects"
            component={ProjectsView}
            title="ProjectsView"
            headerBackTitle="log out"
            options={{
              headerLeft: function Header() {
                return <Logout />;
              },
            }}
          />
          <Stack.Screen name="Task List">
            {(props) => {
              const { navigation, route } = props;
              const { user, projectPartition } = route.params;
              return (
                <TasksProvider user={user} projectPartition={projectPartition}>
                  <TasksView navigation={navigation} route={route} />
                </TasksProvider>
              );
            }}
          </Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
          </AuthProvider>*/}
          </View>
  );
};

export default App;
