import React from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-elements';

export const Home = () => {
  return (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
      <Text>HOME</Text>
    </View>
  );
};
