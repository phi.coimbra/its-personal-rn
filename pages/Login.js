import React, { useEffect } from 'react';
import {View} from 'react-native';
import {Text} from 'react-native-elements';
import { useRealm } from '../providers/ReducerProvider';
const { logged } = useRealm();


export const Login = () => {

  useEffect(()=> {
    console.log(logged, 'ok')
  },[logged])
  return (
    <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
      <Text>LOGIN</Text>
    </View>
  );
};
