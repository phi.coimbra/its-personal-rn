

export function appReducer(state, action) {
    console.log('reducer', state, action)
    switch(action.type){
        case 'user' : {
            return state.user
        }
        case 'logIn' : {
            return { 
                ...state,
                user:action.payload
            }
            
        }
        case 'logOut' : {
            return {
                ...state,
                user:null
            }
        }
        case 'user' : {
            return {
                ...state,
                cliente:action.payload
            }
        }
        case 'navigation' : {
            return {
                ...state,
                navigation:action.payload
            }
        }
        case 'navigate' : {
            state.navigation.navigate(action.payload, { screenName: action.payload})
            return {
                ...state,
                navigate:action.payload
            }
        }
        default : {
            return state     
        } 
    }
}

